package com.example.googlemaplerning.evgeny.rxjavalerning

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.os.Looper
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import io.reactivex.*
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.functions.BiFunction
import io.reactivex.internal.operators.observable.ObservableRange
import io.reactivex.observables.ConnectableObservable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*
import java.util.concurrent.Executor
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit

class MainActivity : AppCompatActivity(), PracticeRxJava.PracticeRxJavaListener {

    val practice = PracticeRxJava()

    @SuppressLint("CheckResult")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

//        val observable =
//            Observable.just("1", "12", "123")
//
//        observable.subscribe {
//            Log.d("subscribe 1", it)
//        }
//
//        observable.subscribe {
//            Log.d("subscribe 2", it)
//        }
//
//        observable.subscribe {
//            Log.d("subscribe 3", it)
//        }

        ///////////////////////////

        //Если не уверен, какой шедулер выбрать, пикай computation(), не прогадаешь.
//        Observable.fromCallable {
//            "123"
//        }
//            .observeOn(Schedulers.computation())
//            .subscribe {
//                Log.d("MainActivity", "${Looper.myLooper() == Looper.getMainLooper()}")
//            }
//
//        //Каждый новый subscription будет вызывать создание нового thread'а.
//        Observable.fromCallable {
//            "123"
//        }
//            .observeOn(Schedulers.io())
//            .subscribe {
//                Log.d("MainActivity", "${Looper.myLooper() == Looper.getMainLooper()}")
//            }
//
//        ///////////////////////////
//        Observable.fromCallable {
//            "123"
//        }
//            .subscribe({
//                Log.d("MainActivity", "")
//            }, {
//                Log.d("MainActivity", "")
//            }, {
//                Log.d("MainActivity", "")
//            })


        ///////////////////////////
//        val source = Observable.create<String> { emitter ->
//            emitter.onNext("1")
//            emitter.onNext("2")
//            emitter.onNext("3")
//        }
//
//        source.subscribe {
//            Log.d("MainActivity", it)
//        }
//
//        val sourceMap = source.map { it.length }

        //cold and hot
        //Обсерваблы, которые эмитят конечные данные чаще всего cold
        //(отрабатывают тогда, когда на них подписались). Горячие похожи
        // на broadcast, вещают всем подписчикам. Работает, как радио.
        // Если пропустил трэк, его уже не услышишь. Горячие чаще всего
        // отображают события, нежели какие-то данные.
        //пример горячего обсервебала
//        val hot: ConnectableObservable<Long> =
//            ConnectableObservable.interval(1000, TimeUnit.MILLISECONDS).publish()

        val hot2: ConnectableObservable<String> = Observable.fromCallable {
            "123"
        }.publish()

        hot2.subscribe {
            Log.d("MainActivity", "ConnectableObservable hot 1")
        }
        hot2.connect()
        hot2.connect() //только один раз

//        Observable.just("2131", "asd", "asd", null)
//            .subscribe({
//                Log.d("MainActivity", it)
//            }, {
//                it.printStackTrace()
//            })
//        Observable.just(getServiceToken())
//            .subscribe({
//                Log.d("MainActivity", it)
//            }, {
//                it.printStackTrace()      //never called
//            })

//        Observable.fromCallable {
//            getServiceToken()
//        }
//            .subscribe({
//                Log.d("MainActivity", it)
//            }, {
//                it.printStackTrace()
//            })


        //////////////////////////нихусе
//        var maxCount = 5
//        val source3 = Observable.defer {
//            Observable.range(1, maxCount)
//        }
//
//        source3.subscribe {
//            Log.d("MainActivity", "Observer 1 $it")
//        }
//
//        maxCount = 10
//        source3.subscribe {
//            Log.d("MainActivity", "Observer 2 $it")
//        }


        //////////////////////////
//        Observable.just("123", "123", "123")
//            .single("1")
//            .subscribe ({
//                Log.d("MainActivity", it)
//            }, {
//                it.printStackTrace() //call this
//            })

        //Напомню, что в Maybe может прийти либо один onNext, либо onComplete,
        // либо OnError. После этого Maybe считается завершенным.
        //Третий тип — Maybe. Это новый тип по сравнению с RxJava 1.
        // Он может либо содержать элемент, либо выдать ошибку, либо
        // не содержать данных — этакий реактивный Optional.
//        Maybe.fromCallable {
//            var str : String? = null
//            str
//        }
//            .subscribe ({
//                Log.d("MainActivity", "subscribe")
//            }, {
//                Log.d("MainActivity", "error")
//            }, {
//                Log.d("MainActivity", "complete")
//            })

        //Второй тип называется Completable. Он похож на void-метод.
        // Он либо успешно завершает свою работу без каких-либо данных,
        // либо бросает исключение. То есть это некий кусок кода, который
        // можно запустить, и он либо успешно выполнится, либо завершится сбоем.


        //////////////
//        val dispos = Observable.just("123", "123")
//            .subscribe { _ ->
//            }
//        dispos.dispose()
//
//
//        //////////////для диспоси
//        val compositeDisposable = CompositeDisposable()
//        compositeDisposable += Observable.just("123")
//            .subscribe {
//            }
//        compositeDisposable += Observable.just("1234")
//            .subscribe {
//            }
//        compositeDisposable.dispose()


        //////////////
        //аналогично skip
//        Observable.range(0, 100)
//            .skipWhile {
//                it < 5
//            }
//            .subscribe {
//                Log.d("MainActivity", "$it")
//            }


        //////////////
//        Observable.just(1, 1, 2, 2, 3, 3, 1)
//            .distinctUntilChanged()
//            .subscribe {
//                Log.d("MainActivity", "distinctUntilChanged $it")
//            }


        //////////////
//        Observable.fromIterable(listOf("123", "1234", "12345"))
//            .elementAtOrError(2)
//            .subscribe({
//                Log.d("MainActivity", "elementAt $it")
//            }, {
//                it.printStackTrace()
//            })


        //////////////
        //позволяет нам впихнуть какой-нибудь эмишн, с которого начинаются другие эмишны
//        Observable.fromIterable(listOf("1", "2", "3"))
//            .subscribeOn(Schedulers.computation())
//            .observeOn(AndroidSchedulers.mainThread())
//            .startWith {
//                Log.d("MainActivity", "startWith ${Looper.myLooper() == Looper.getMainLooper()}")
//                it.onComplete()
//                it.onNext("0")
//            }
//            .subscribe {
//                Log.d("MainActivity", "startWith ${Looper.myLooper() == Looper.getMainLooper()}")
//                Log.d("MainActivity", "startWith $it")
//            }


        //////////////
//        Observable.fromIterable(listOf("1", "2", "3"))
//            .delay(1000, TimeUnit.MILLISECONDS)
//            .subscribe {
//                Log.d("MainActivity", "delay $it")
//            }


        //rolling аггрегатор. Аккумулирует каждый эмишн и добавляет его к следующему.
//        Observable.fromIterable(listOf("1", "2", "3", "4"))
//            .scan { t1: String, t2: String ->
//                "$t1 $t2"
//            }
//            .subscribe {
//                Log.d("MainActivity", "scan $it")
//            }


        /////////////////////
//        Observable.fromIterable(listOf("1", "2", "3", "4"))
//            .doOnNext {
//                throw Exception()
//            }
//            .onErrorReturn {
//                it.printStackTrace()
//                "error"
//            }
//            .subscribe {
//                Log.d("MainActivity", "onErrorReturn $it")
//            }


        //имеем шанс только на еще один запрос
//        request()
//            .onErrorResumeNext (request().repeat(1))
//            .onErrorResumeNext (request().repeat(1))
//            .subscribe ({
//            }, {
//                Log.d("MainActivity", "запрос сломался")
//            })


        //отработает три раза
//        request()
//            .retry(3)
//            .subscribe ({
//
//            }, {
//
//            })


        //подпишутся на все источники одновременно, но элементы будут
        // эмититься в одном потоке
        //Не стоит полагаться на порядок эмитов в случае с merge
//        Observable.merge(
//            getSourceToMerge("1", "2", "3"),
//            getSourceToMerge("4", "5", "6"),
//            getSourceToMerge("7", "8", "9"),
//            getSourceToMerge("10", "11")
////            getSourceToMerge("4", "5", "6") слишком много
//        )
//            .subscribeOn(Schedulers.io())
//            .observeOn(AndroidSchedulers.mainThread())
//            .subscribe {
//                Log.d("MainActivity", "merge ${it.toString()}")
//            }

//        getSourceToMerge("1", "2", "3").mergeWith(
//            getSourceToMerge("4", "5", "6").mergeWith(
//                getSourceToMerge("7", "8", "9").mergeWith(
//                    getSourceToMerge("10", "11")
//                )
//            )
//        ).subscribe {
//            Log.d("MainActivity", "merge ${it.toString()}")
//        }


        //Объеденит эмишены разных Observable и будет пулять их поочерёдно,
        // переключаясь к следующему источнику только после того, как текущий
        // вызовет onComplete().
//        Observable.concat(
//            getSourceToMerge("1", "2", "3"),
//            getSourceToMerge("4", "5", "6"),
//            getSourceToMerge("7", "8", "9"),
//            getSourceToMerge("10", "11")
//        )
//            .subscribe {
//                Log.d("MainActivity", "concat ${it.toString()}")
//            }

//        val sourceInterval = Observable.interval(1, TimeUnit.SECONDS)
//            .take(2)
//            .map {
//                Log.d("MainActivity", "$it interval SECONDS")
//            }
//
//        val sourceInterval2 = Observable.interval(500, TimeUnit.MILLISECONDS)
//            .take(3)
//            .map {
//                Log.d("MainActivity", "$it interval MILLISECONDS")
//            }
//
        //разница между merge и concat
//        Observable.merge(sourceInterval, sourceInterval2)
//            .subscribe {
//            }

//        Observable.just("1", "2", "3")
//            .concatWith(Observable.just("4", "5", "6"))
//            .subscribe {
//
//            }

        //про flatMap concatMap concatMapEager
        //https://www.nurkiewicz.com/2017/08/flatmap-vs-concatmap-vs-concatmapeager.html

        ////////////////////////////////////
//        val sourceAmb =
//            Observable.just("123", "1234", "12345")
//
//        val sourceAmb2 =
//            Observable.just("123-", "1234--", "12345---")
//
//        //выпускает эмишены первого заэмитившего Observable. Остальные в свою очередь высвобождаются.
//        // Первым считается такой обзёрвабл, чьи эмишены проходят через данное звено в
//        // чейне(толкование автора конспекта). Это полезно, когда у вас несколько источников
//        // одних и тех же данных, поэтому вам пофиг откуда их брать, лишь бы побыстрее.
//        Observable.amb(Arrays.asList(sourceAmb, sourceAmb2))
//            .subscribe {
//                Log.d("MainActivity", "$it amb")
//            }


        ////////////////////////////////////zip
        // Типы входных эмишенов могут быть разные, но мы можем объеденить и их тоже.
        var sourceZip1 = Observable.just("zip 1", "zip 2", "zip 3")
        var sourceZip2 = Observable.range(1, 5)


//        Observable.zip(sourceZip1, sourceZip2,
//            BiFunction<String, Int, String> { emissionO1, emissionO2 ->
//                "$emissionO1 $emissionO2"
//            }
//        )
//            .subscribe({
//                Log.d("MainActivity", it)
//            }, {
//                Log.d("MainActivity", "Error zip")
//                it.printStackTrace()
//            })

        //если мы в чейне запускаем другой Observable запрос и он падает с error
//        practice.testChein()


        //если у нас два источника которые эмитят с разным промежутком и один из источников крашится
//        practice.testMerge()


//        practice.testInterval(this)
//        txvClickNewActivity.setOnClickListener {
//            startActivity(Intent(this, MainActivity::class.java))
//            finish()
//        }

        ////////////////////////////////////combineLatest
        //что-то похожее на zip(), только отрабатывает с самым последним элементом, выпущенным из
        // любого другого источника. Он не блочит и не ставит в очередь эмишены, которым не нашлось
        // пары, но кэширует их и зипует с последним.

//        val combineSource =
//            Observable.just("combineSource1 1", "combineSource1 2", "combineSource1 3")
//        val combineSource2 =
//            Observable.just("combineSource2 1", "combineSource2 2", "combineSource2 3")
//        Observable.combineLatest(combineSource, combineSource2,
//            BiFunction { t1: String, t2: String -> "$t1 $t2" })
//            .subscribe {
//                Log.d("MainActivity", it)
//            }


        ////////////////////////////////////withLatestFrom наоборот
//        val combineSource =
//            Observable.just("combineSource1 1", "combineSource1 2", "combineSource1 3")
//        val combineSource2 =
//            Observable.just("combineSource2 1", "combineSource2 2", "combineSource2 3")
//
//        combineSource.withLatestFrom(combineSource2,  BiFunction { t1: String, t2: String -> "$t1 $t2" })
//            .subscribe {
//                Log.d("MainActivity", it)
//            }


        ////////////////////////////////////groupBy
//        val sourceGroup = Observable.just("1", "22", "333", "1", "333")
//        sourceGroup.groupBy {
//            it.length
//        }
//            .flatMapSingle {
//                it.toList()
//            }
//            .subscribe {
//                Log.d("MainActivity", it.toString())
//            }

        ////////////////////////////////////
        //после вызова connect, будет ли имитить на новых подписчиков?
//        practice.testConnectableObservable()


        ////////////////////////////////////refCount можно переиспользовать Observable
//        val source = Observable.interval(1, TimeUnit.SECONDS)
//            .publish()
//            .refCount()
//
//        source.take(3)
//            .subscribe {
//                Log.d("MainActivity", "source1 ${it}")
//            }
//
//        Thread.sleep(5000)
//
//        source.take(2)
//            .subscribe {
//                Log.d("MainActivity", "source2 ${it}")
//            }

        ////////////////////////////////////Subject
//        val subjectSource = PublishSubject.create<String>()
//
//
//
//        subjectSource
//            .subscribe {
//                Log.d("MainActivity", "source1 $it")
//            }
//
//        subjectSource
//            .subscribe {
//                Log.d("MainActivity", "source2 $it")
//            }
//
//
//        subjectSource.onNext("1")
//        subjectSource.onNext("2")
//        subjectSource.onNext("3")

        ////////////////////////////////////
//        val myThreadFactory = MyThreadFactory()
//        val myExecutor = Executors.newCachedThreadPool(myThreadFactory)
//        val scheduler = Schedulers.from(myExecutor)
//
//        Observable.fromCallable {
//            "12312"
//        }
//            .subscribeOn(scheduler)
//            .observeOn(AndroidSchedulers.mainThread())
//            .subscribe({
//                Log.d("MainActivity", "$it")
//            }, {
//                it.printStackTrace()
//                Log.d("MainActivity", "error")
//            })
//
//
//        Observable.fromCallable {
//            "12312"
//        }
//            .subscribeOn(scheduler)
//            .observeOn(AndroidSchedulers.mainThread())
//            .subscribe({
//                Log.d("MainActivity", "$it")
//            }, {
//                it.printStackTrace()
//                Log.d("MainActivity", "error")
//            })


        //Если нужно иметь дело с высоконагруженными Observable'ами, которые эмитят 10К и
        // более элементов, то стоит работать с Flowable.


        //выполнится за 10 секунд
//        Observable.range(1, 10)
//            .map(i -> /*что-то делается за секунду*/)
//            .subscribe(..)

        //выполнится за 1 секунду, потому что flatMap смерджит обсервебелы
        //computation раскидает по потокам
//        Observable.range(1, 10)
//            .flatMap(i -> Observable.just(i)
//                          .subscribeOn(Schedulers.computation())
//                          .map(i2 -> /*что-то делается за секунду*/))
//        .subscribe(..)

        //получить количество ядер процессора
//        int coreCount = Runtime.getRuntime().availableProcessors()


        ////////////////////////////////////
//        Observable.range(1, 50)
//            .buffer(8)
//            .subscribe {
//                Log.d("MainActivity", "$it")
//            }


        ////////////////////////////////////switchMap
        //дропает тех кто задерживается
//        Observable.just("1", "2", "3", "4")
//            .switchMap {
//                Observable.just(it)
//                    .delay(300, TimeUnit.MILLISECONDS)
//            }
//            .subscribe {
//                Log.d("MainActivity", "$it")
//            }

//        //если будет null цепочка упадет
//        Observable.fromCallable {
//            "123"
//        }
//            .map {
//                var str : String? = it
//                str = null
//                str
//            }
//            .subscribe ({
//                Log.d("MainActivity", "$it")
//            }, {
//                it.printStackTrace()
//                Log.d("MainActivity", "error null")
//            })

        practice.testAutoConnect()

    }


    private fun getSourceToMerge(vararg arg: String): Observable<List<String>> {
        return Observable.just(arg.toList())
    }


    private fun request(): Observable<String> {
        return Observable.fromCallable {
            Log.d("MainActivity", "запрос")
            Log.d("MainActivity", "exception")
            throw Exception()
            "запрос"
        }
    }


    private fun getServiceToken(): String {
        throw Exception()
        return "123"
    }

    override fun foo() {}
}

private operator fun CompositeDisposable.plusAssign(subscribe: Disposable) {
    add(subscribe)
}
