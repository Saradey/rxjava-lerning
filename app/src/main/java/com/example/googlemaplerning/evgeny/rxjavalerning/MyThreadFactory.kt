package com.example.googlemaplerning.evgeny.rxjavalerning

import java.util.concurrent.ThreadFactory

class MyThreadFactory : ThreadFactory {

    override fun newThread(p0: Runnable): Thread {
        return MyThread(p0)
    }

}