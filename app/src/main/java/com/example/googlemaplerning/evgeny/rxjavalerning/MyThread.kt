package com.example.googlemaplerning.evgeny.rxjavalerning

import android.util.Log

class MyThread(runnable: Runnable) : Thread(runnable) {

    init {
        Log.d("MainActivity", "createNewMyThread ${this.name}")
    }


    override fun start() {
        Log.d("MainActivity", "startNewMyThread ${this.name}")
        super.start()
    }


    override fun interrupt() {
        Log.d("MainActivity", "destroyNewMyThread ${this.name}")
        super.interrupt()
    }
}