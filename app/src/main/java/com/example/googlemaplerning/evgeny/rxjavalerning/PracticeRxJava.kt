package com.example.googlemaplerning.evgeny.rxjavalerning

import android.util.Log
import io.reactivex.Observable
import io.reactivex.observables.ConnectableObservable
import io.reactivex.subjects.PublishSubject
import java.lang.Exception
import java.util.concurrent.TimeUnit
import javax.security.auth.Subject

class PracticeRxJava {


    //если мы в чейне запускаем другой Observable запрос и он падает с error
    //ответ: чейн отработает нормально, а другой чейн упадет
    fun testChein() {
        Observable.fromCallable {
            arrayListOf("123", "1234", "12345")
        }
            .doOnNext {
                requestException()
            }
            .subscribe({
                Log.d("MainActivity", "subscribe first")
            }, {
                Log.d("MainActivity", "error first")
            })
    }


    private fun requestException() {
        Observable.fromCallable {}
            .doOnNext {
                throw Exception()
            }
            .subscribe({
                Log.d("MainActivity", "subscribe second")
            }, {
                Log.d("MainActivity", "error second")
            })
    }


    //если у нас два источника которые эмитят с разным промежутком и один из источников крашится
    //ответ: крашится весь чейн
    fun testMerge() {
        val source1 = Observable.just("source1 1", "source1 2", "source1 3", "source1 4")
            .delay(500, TimeUnit.MILLISECONDS)
            .doOnNext {
                if (it == "source1 3")
                    throw Exception()
            }

        val source2 = Observable.just("source2 5", "source2 6", "source2 7", "source2 8")
            .delay(1000, TimeUnit.MILLISECONDS)

        Observable.merge(source1, source2)
            .subscribe({
                Log.d("MainActivity", "$it")
            }, {
                Log.d("MainActivity", "error second")
            })
    }


    //что если запущен интервал и у нас в чейне скрытая ссылка на активити
    //будет ли утечка памяти?
    //ответ: утечка будет
    fun testInterval(listener: PracticeRxJavaListener) {
        Observable.interval(1000, TimeUnit.MILLISECONDS)
            .subscribe {
                Log.d("MainActivity", "$it")
                listener.foo()
            }
    }

    interface PracticeRxJavaListener {
        fun foo()
    }


    //посмотреть как ConnectableObservable работает с interval
    //после вызова connect, будет ли имитить на новых подписчиков?
    //ответ: будет!
    fun testConnectableObservable() {
        val source = ConnectableObservable.interval(1000, TimeUnit.MILLISECONDS)
            .map {
                it * 10
            }
            .publish()

        val consumer = { lon: Long ->
            Log.d("MainActivity", "source1 $lon")
            Unit
        }

        source
            .take(3)
            .subscribe(consumer)

        source
            .take(3)
            .subscribe {
                Log.d("MainActivity", "source2 $it")
            }
        source.connect()

        Thread.sleep(5000)


        source
            .subscribe {
                Log.d("MainActivity", "source3 $it")
            }
    }



    fun testAutoConnect() {
//        val source = Observable.just("1", "2", "3").publish()
//            .autoConnect(1)
        val source = Observable.just("1", "2", "3").publish()
            .autoConnect(2)


        source.subscribe {
            Log.d("MainActivity", "source1 $it")
        }

        source.subscribe {
            Log.d("MainActivity", "source2 $it")
        }
    }


}